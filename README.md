# assignment

## Name
School Admin Web App.

## Description
A system where Teachers can perform administrative functions for their students. Teachers and students are identified by their email addresses.

## Visuals
Landing Page:
Select Admin or School Portal. (Admin Portal is exclusive to Teachers only.)
![ ALT](/samples/Landing_Page.png)

Admin Portal | Class Management Page:
Assign Students or Teachers (Users) to Classes.
![ ALT](/samples/ClassManagement_Page.png)

Admin Portal | User Management Page:
Create new Users, assign their permissions and add to a class.
![ ALT](/samples/AddUserToSchoolAndClass.png)



## Installation
Download or clone the repository then run these commands:
  - node -v (Node version must be 16.14.2)
  - npm ci
  - cd public
  - npm ci

For development, execute the following inside their directiories.
  - npm start ("root" directory:)
  - npm run watch ("root/public" directory)

This will build the ReactJS into "public/dist/" and start the local server on: http://localhost:8080/




## Usage
### Admin Portal Web App
1. As a teacher, I want to register one or more students to a specified teacher<br />
Endpoint: POST /api/admin/addUser<br />
Request: form: {<br />
    &ensp;SchoolFID, RoleFID, contact, email, name, password<br />
}<br />
Web App: Admin Portal

2. As a teacher, I want to retrieve a list of students common to a given list of teachers (i.e. retrieve students who are registered to ALL of the given teachers).<br />
Endpoint: POST /api/teacher/req_usersSameClassAs_UserID<br />
Request: form: {<br />
    &ensp;UserID<br />
}

### Student Portal Web App
3. As a teacher, I want to suspend a specified student.<br />
Endpoint: POST API/admin/suspend_addUser<br />
Request: form: {<br />
    &ensp;UserID,<br />
    &ensp;ClassID<br />
}

4. As a teacher, I want to retrieve a list of students who can receive a given notification.<br />
Endpoint: POST API/teacher/req_studentsEligibleforNotifications_byTeacher<br />
Request: form: {<br />
    &ensp;TeacherID,<br />
}

### Postman
In directory: "root/Postman/", there is a file: "SchoolAdmin.postman_collection.json".<br />
Download and import into Postman.
![ ALT](/samples/Postman_SchoolAdmin.png)

As shown in the image above, there are 4 subfolders for 4 user stories:
1. As a teacher, I want to register one or more students to a specified teacher.
  - Folder: "Register"
2. As a teacher, I want to retrieve a list of students common to a given list of teachers (i.e. retrieve students who are registered to ALL of the given teachers).
  - Folder: "Common Students"
3. As a teacher, I want to suspend a specified student.
  - Folder: "Suspend"
4. As a teacher, I want to retrieve a list of students who can receive a given notification.
  - Folder: "Retrieve For Notifications"

![ ALT](/samples/Postman_RFN.png)
(e.g. "SchoolAdmin/Retrieve For Notifications/DEV/Suspend(tagged does not exist)")<br />
The image above shows one of the requests fully configured to test the scenario described by its name.


## Project status
There is a lot left to be done before this Notification System is functional. However, the 4 User Stories are satisfied.

The design of the project is for scalability. Changing database, or implementing new features. I enjoy the "plug n play" concept as it makes future development and maintenance streamlined.
