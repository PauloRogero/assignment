const QE = require("./QueryEngine")

describe('properly connects to Azure MySQL', () => {
	test("Should return true if can reach Azure MySQL", async () => {
		const test = await QE.initTest()
		expect(test).toBe(true)
	})
})