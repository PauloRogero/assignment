import React from 'react';
import { Link } from "react-router-dom";
import {
	Card,
	Grid,
	Typography,
	List,
	CircularProgress
} from '@mui/material';

import { makeStyles } from '@material-ui/core/styles';

import Item from './Item';
import QueryEngine from '../../../../../Queries/Portal';

const useStyles=makeStyles((theme)=>({
	loadingDiv:{
		height:"calc(50vh - (64px) - (20px + 3px))",
		border:"1px solid #eeeeee",
		textAlign:"center"
	},
	loading:{
		marginTop:"calc(25vh - 20px - 64px)"
	}
}));

export default function ClassesList(props) {
	// props.classesData
	// props.user
	// props.account
	// props.school
	// props.token

	const classes=useStyles();

	const [students, setStudents] = React.useState(null)

	const template_loading=(
		<Card className={classes.loadingDiv}>
			<div className={classes.loading}>
				<CircularProgress />
				<div>Loading...</div>
			</div>
        </Card>
	)

	React.useEffect(() => {
		if(props.user.RoleFID < 3){
			QueryEngine.teacher.req_studentsEligibleforNotifications_byTeacher(props.user.UserID, res => {
				setStudents(res)
			}, props.token)
		}else{
			setStudents(false)
		}
		
	}, [props.classesData])
	
    return (
		<React.Fragment>
			{
				students === null?
				template_loading
				:
				students?
					<div style={{
						padding: 8
					}}>
						<Grid container style={{
							background: '#fff',
							padding: 16,
							borderRadius: 5
						}}>
							<Grid item xs={12}>
								<Typography variant='h6'>
									Get Students Eligible for Notifications:
								</Typography>
							</Grid>
							<Grid item xs={12}>
								{/* <List>
									{
										props.classesData.map(Class => {
											return (
												<Item
													data={Class}
												/>
											)
										})
									}
								</List> */}
								{/* <pre>{JSON.stringify(students, null, 2)}</pre> */}
								{
									Object.keys(students).map(classObj => {
										return (
											<List>
												{
													students[classObj].map(student => {
														return (
															<Item
																data={student}
															/>
														)
													})
												}
											</List>
										)
									})
								}
							</Grid>
						</Grid>
					</div>
					:
					''
			}
		</React.Fragment>
    )
}
