import React from 'react';
import { Link } from "react-router-dom";
import {
	Grid,
	List,
	ListItem,
	ListItemAvatar,
	ListItemText,
	Avatar,
	IconButton
} from '@mui/material';

import NavigateNextIcon from '@mui/icons-material/NavigateNext';

export default function ClassesList(props) {
	// props.data
	
    return (
		<ListItem
			secondaryAction={
				<IconButton
					edge="end"
					aria-label="delete"
					component={Link}
					to="/portal/class"
				>
					<NavigateNextIcon />
				</IconButton>
			}
		>
			<ListItemAvatar>
				<Avatar>
					{props.data.UserID}
				</Avatar>
			</ListItemAvatar>
			<ListItemText
				primary={props.data.name}
				secondary={
					"Last Modified: " + new Date(props.data.lastModified).toLocaleString('en-SG', {
						timeZone: 'Asia/Singapore',
						hour: '2-digit',
						minute:'2-digit',
						second:'2-digit'
					})
				}
			/>
		</ListItem>
    )
}
