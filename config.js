// Production Branch
const fs = require('fs');

// SQL Config
const sqlCFG = {
    host: 'schooladmin.mysql.database.azure.com',
    user: 'paulorogero',
    password: 'Omnitribe@2023!',
    database: 'school1',
    port: 3306,
    ssl: {
        rejectUnauthorized: true,
        // ca: fs.readFileSync('./libs/CERT/BaltimoreCyberTrustRoot.crt.pem')
        ca: fs.readFileSync('./libs/CERT/DigiCertGlobalRootCA.crt.pem')
    }
}

//Helmet Config
const cspSrc = [
    "'self'",
    "'unsafe-eval'",
    "'unsafe-inline'",
    "'unsafe-hashes'",
    "'unsafe-inline'",
    "data:",
    "blob:",
    "wss:",
    '*.rogeropaulo.com',
    "*.googleapis.com",
    "schooladmin.mysql.database.azure.com"
];
const contentSecurityPolicy = {
    useDefaults: true,
    directives: {
        childSrc: cspSrc,
        workerSrc: cspSrc,
        connectSrc: cspSrc,
        scriptSrc: cspSrc,
        styleSrc: cspSrc,
        defaultSrc: [
            '*.rogeropaulo.com',
            "schooladmin.mysql.database.azure.com"
        ],
        imgSrc: [
            "'self'",
            "blob:",
            "data:",
            "images.unsplash.com",
            "schooladmin.mysql.database.azure.com"
        ],
        "frame-ancestors": [
            "*"
        ]
    },
}

//CORS Config
const allowedOrigins = 
    process.env.AWS_LAMBDA_LOG_GROUP_NAME !== '/aws/lambda/schooladmin-prod-api'?
    [
        'http://localhost:8080',
        'https://dev-schooladmin.rogeropaulo.com',
    ]
    :
    [
        'https://schooladmin.rogeropaulo.com'
    ];

//Export Configurations
const config = {
    cspConfig: contentSecurityPolicy,
    allowedOrigins,
    sqlCFG
};

module.exports = config;