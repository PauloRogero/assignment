const QueryEngine = require('../../QueryEngine');

const commonStudents= {
	
	init: (req, res) => {
		let form = req.query
		let conID,
			con = QueryEngine.SQL.initSQL()
		QueryEngine.SQL.connect(con, connection => {
			if(connection.error){
				res.status(500).send({
					message: res.error
				})
			}else{
				conID = connection.connection.threadId
				
				let timeout = setTimeout(() => {
					console.log('timed out...')
				}, 25000)

				console.log("API commonstudents called", req)
				commonStudents.validateTeachers(con, form.teacher, res).then(teachers => {
					console.log("validated teachers!", teachers)
					commonStudents.getTeachersClasses(con, teachers, res).then(classes => {
						console.log("retreived classes!", classes)
						commonStudents.commonStudents(con, classes, res, timeout)
					}).catch(err=>{})
				}).catch(err=>{})

			}
		})
	},

	commonStudents: (con, classes, res, timeout) => {
		console.log("Phase 1 complete!")
		commonStudents.getUsersFromSameClasses(con, classes, res, timeout).then(UserIDs => {
			commonStudents.filterStudents(con, UserIDs, res, timeout).then(students => {
				console.log("Operation completed!", students)
				commonStudents.endConnection(con, timeout, () => {
					res.status(200).send({students})
				})
			})
		})
	},

	validateTeachers: (con, arr, res) => {
		return new Promise((resolve, reject) => {
			console.log("validating teachers...")
			let query = 
				"SELECT "+
					"UserID, "+
					"email "+
				"FROM Users "+
				"WHERE "+
					"{replace} "+
					"AND isDeleted is FALSE"
			;
			if(Array.isArray(arr)){
				let UserIDs = arr.map(email => {
					return "email = '"+email+"'"
				})
				query = query.replace("{replace}", "( "+UserIDs.join(" OR ")+" ) ")
			}else{
				query = query.replace("{replace}", " email = '"+arr+"' ")
			}
			
			console.log("query: "+ query)
			QueryEngine.SQL.query(con, query, teachers => {
				console.log("result: ", teachers)
				if(teachers.result.length === 0){
					commonStudents.endConnection(con, undefined, () => {
						reject(false)
						res.status(409).send({
							message: "Request denied. Teacher does not exist!"
						})
					})
				}else{
					resolve(teachers.result)
				}

			})
		})
	},
	getTeachersClasses: (con, arr, res) => {
		return new Promise((resolve, reject) => {
			console.log("getting classes...")
			let teachersClasses = 
				"SELECT "+
					"ClassFID "+
				"FROM ClassMembers "+
				"WHERE "+
					"( {replace} ) "+
					"AND isDeleted is FALSE"
			;
			let UserIDs = arr.map(teacher => {
				return "UserFID = '"+teacher.UserID+"' "
			})
			teachersClasses = teachersClasses.replace("{replace}", UserIDs.join(" OR "))
			console.log("query: "+ teachersClasses)
			QueryEngine.SQL.query(con, teachersClasses, classes => {
				if(classes.result.length === 0){
					commonStudents.endConnection(con, undefined, () => {
						reject(false)
						res.status(409).send({
							message: "Request denied. Teacher does not have classes!"
						})
					})
				}else{
					resolve(classes.result)
				}

			})
		})
	},

	getUsersFromSameClasses: (con, classes, res, timeout) => {
		return new Promise(resolve => {
			console.log("getting users from shared classes...")
			let query = 
				"SELECT "+
					"UserFID "+
				"FROM ClassMembers "+
				"WHERE "+
					"( {replace} ) "+
					"AND isDeleted is FALSE "+
				"GROUP BY UserFID"
			;
			let ClassIDs = classes.map(c => {
				return "ClassFID = '"+c.ClassFID+"'"
			})
			query = query.replace("{replace}", ClassIDs.join(" OR "))
			console.log("query: "+ query)
			QueryEngine.SQL.query(con, query, users => {
				if(users.result.length === 0){
					commonStudents.endConnection(con, timeout, () => {
						res.status(409).send({
							message: "Request denied. Classes are empty!"
						})
					})
				}else{
					resolve(users.result)
				}

			})
		})
	},
	filterStudents: (con, arr, res, timeout) => {
		return new Promise(resolve => {
			console.log("cross referencing list with Users for students only...")
			let query = 
				"SELECT "+
					"email "+
				"FROM Users "+
				"WHERE "+
					"( {replace} ) "+
					"AND isDeleted is FALSE "+
					"AND RoleFID > 2"
			;
			let UserIDs = arr.map(c => {
				return "UserID = '"+c.UserFID+"'"
			})
			query = query.replace("{replace}", UserIDs.join(" OR "))
			console.log("query: "+ query)
			QueryEngine.SQL.query(con, query, students => {
				if(students.result.length === 0){
					commonStudents.endConnection(con, timeout, () => {
						res.status(409).send({
							message: "Request denied. No students!"
						})
					})
				}else{
					resolve(students.result.map(student => {
						return student.email
					}))
				}

			})
		})
	},

	endConnection: (con, timeout, handle) => {
		QueryEngine.SQL.end(con, () => {
			handle()
			if(timeout) clearTimeout(timeout)
		})
	}
	
};

module.exports = commonStudents;
