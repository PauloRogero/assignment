const QueryEngine = require('../../QueryEngine');
const commonStudents = require('./commonStudents');

const retrievefornotifications= {
	
	init: (req, res) => {
		let form = req.body,
			conID,
			con = QueryEngine.SQL.initSQL(),
			email = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi,
			tagged = form.notification.match(email)
		QueryEngine.SQL.connect(con, async connection => {
			if(connection.error){
				res.status(500).send({
					message: res.error
				})
			}else{
				conID = connection.connection.threadId

				let pre = {
					classes: null,
					students: null
				}
				
				retrievefornotifications.validateTeacher(con, form.teacher, res).then(teacher => {
					let SchoolID = teacher[0].SchoolFID
					retrievefornotifications.validateTeachersClasses(con, teacher[0].UserID, res).then(classes => {
						pre.classes = classes
						retrievefornotifications.initialized(con, pre, res)
					}).catch(err=>{})

					retrievefornotifications.validateStudents(con, SchoolID, tagged).then(students => {
						pre.students = students
						retrievefornotifications.initialized(con, pre, res)
					})
				}).catch(err=>{})

			}
		})
	},

	initialized: (con, {classes, students}, res) => {
		if(classes && students != null){
			commonStudents.getUsersFromSameClasses(con, classes, res, undefined).then(UserIDs => {
				console.log(UserIDs)
				commonStudents.filterStudents(con, UserIDs, res, undefined).then(s => {
					QueryEngine.SQL.end(con, () => {
						res.status(200).send(retrievefornotifications.filterTagged(s, students))
					})
				})
			})
		}
	},

	validateTeacher: async (con, email, res) => {
		return new Promise((resolve, reject) => {
			let teacherExists =
				"SELECT "+
					"UserID, "+
					"SchoolFID "+
				"FROM Users "+
				"WHERE "+
					"email = '" + email + "' "+
					"AND RoleFID < 3"
			;
			QueryEngine.SQL.query(con, teacherExists, teacher => {

				if(teacher.result.length === 0){
					reject(false)
					QueryEngine.SQL.end(con, () => {
						res.status(409).send({
							message: "Notification denied. Teacher does not exist!"
						})
					})
				}else{
					resolve(teacher.result)
				}

			})
		})
	},
	validateTeachersClasses: async (con, UserID, res) => {
		return new Promise((resolve, reject) => {
			let teachersClasses = 
				"SELECT "+
					"ClassFID "+
				"FROM ClassMembers "+
				"WHERE "+
					"UserFID = '" + UserID + "' "+
					"AND isDeleted is FALSE"
			;
			QueryEngine.SQL.query(con, teachersClasses, classes => {
				
				if(classes.result.length === 0){
					reject(false)
					QueryEngine.SQL.end(con, () => {
						res.status(409).send({
							message: "Notification denied. Teacher does not have classes!"
						})
					})
				}else{
					resolve(classes.result)
				}

			})
		})
	},

	validateStudents: async (con, SchoolID, arr) => {
		return new Promise(resolve => {
			if(arr){
				let status = {
					totalLength: arr.length,
					checkedStudents: []
				}
				arr.forEach(i => {
					let studentExists =
						"SELECT "+
							"UserID, "+
							"email "+
						"FROM Users "+
						"WHERE "+
							"email = '" + i + "' "+
							"AND SchoolFID = '" + SchoolID + "' "+
							"AND RoleFID > 2"
					;
					QueryEngine.SQL.query(con, studentExists, student => {
	
						if(student.result.length === 0){
							status.checkedStudents.push({
								error: "Does not exist",
								email: i
							})
						}else{
							status.checkedStudents.push({
								...student.result[0]
							})
						}
						retrievefornotifications.handle_validateStudents(status, resolve)
	
					})
				})
			}else{
				resolve(false)
			}
		})
	},
	handle_validateStudents: ({totalLength, checkedStudents}, resolve) => {
		if(totalLength === checkedStudents.length){
			resolve(checkedStudents)
		}
	},

	filterTagged: (emails, students) => {
		console.log("yo", emails, students)
		let temp = {}
		emails.forEach(email => {
			temp[email] = true
		})
		students && students.forEach(student => {
			if(!student.error){
				if(!temp[student.email]){
					temp[student.email] = true
				}
			}else{
				temp[student.email] = false
			}
		})
		let result = []
		Object.keys(temp).map(i => {
			if (temp[i]) result.push(i)
		})
		return result
	}
	
};

module.exports = retrievefornotifications;
