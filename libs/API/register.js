const QueryEngine = require('../../QueryEngine');

const register= {
	
	init: (req, res) => {
		let form = req.body
		let conID,
			con = QueryEngine.SQL.initSQL()
		QueryEngine.SQL.connect(con, async connection => {
			if(connection.error){
				res.status(500).send({
					message: res.error
				})
			}else{
				conID = connection.connection.threadId

				let pre = {
					classes: null,
					students: null
				}
				
				register.validateTeacher(con, form.teacher, res).then(teacher => {
					pre.SchoolID = teacher[0].SchoolFID
					register.validateTeachersClasses(con, teacher[0].UserID, res).then(classes => {
						pre.classes = classes
						register.register(con, pre, res)
					}).catch(err=>{})
				}).catch(err=>{})

				register.validateStudents(con, form.students).then(students => {
					pre.students = students
					register.register(con, pre, res)
				})

			}
		})
	},

	register: (con, {SchoolID, classes, students}, res) => {
		if(classes && students){
			register.createStudents(con, SchoolID, students).then(studentIDs => {
				register.registerToTeacher(con, classes, studentIDs).then(() => {
					QueryEngine.SQL.end(con, () => {
						res.sendStatus(204)
					})
				})
			})
		}
	},

	validateTeacher: async (con, email, res) => {
		return new Promise((resolve, reject) => {
			let teacherExists =
				"SELECT "+
					"UserID, "+
					"SchoolFID "+
				"FROM Users "+
				"WHERE "+
					"email = '" + email + "' "+
					"AND RoleFID < 3"
			;
			QueryEngine.SQL.query(con, teacherExists, teacher => {

				if(teacher.result.length === 0){
					reject(false)
					QueryEngine.SQL.end(con, () => {
						res.status(409).send({
							message: "Registration denied. Teacher does not exist!"
						})
					})
				}else{
					resolve(teacher.result)
				}

			})
		})
	},
	validateTeachersClasses: async (con, UserID, res) => {
		return new Promise((resolve, reject) => {
			let teachersClasses = 
				"SELECT "+
					"ClassFID "+
				"FROM ClassMembers "+
				"WHERE "+
					"UserFID = '" + UserID + "' "+
					"AND isDeleted is FALSE"
			;
			QueryEngine.SQL.query(con, teachersClasses, classes => {
				
				if(classes.result.length === 0){
					reject(false)
					QueryEngine.SQL.end(con, () => {
						res.status(409).send({
							message: "Registration denied. Teacher does not have classes!"
						})
					})
				}else{
					resolve(classes.result)
				}

			})
		})
	},

	validateStudents: async (con, arr) => {
		return new Promise(resolve => {
			let status = {
				totalLength: arr.length,
				checkedStudents: []
			}
			arr.forEach(i => {
				let studentExists =
					"SELECT "+
						"UserID "+
					"FROM Users "+
					"WHERE "+
						"email = '" + i + "' "+
						"AND RoleFID > 2"
				;
				QueryEngine.SQL.query(con, studentExists, student => {

					if(student.result.length === 0){
						status.checkedStudents.push({
							email: i
						})
					}else{
						status.checkedStudents.push({
							...student.result[0]
						})
					}
					register.handle_validateStudents(status, resolve)

				})
			})
		})
	},
	handle_validateStudents: ({totalLength, checkedStudents}, resolve) => {
		if(totalLength === checkedStudents.length){
			resolve(checkedStudents)
		}
	},
	createStudents: (con, SchoolID, students) => {
		return new Promise(async resolve => {
			let result = await Promise.all(
				students.map(async student => {
					let UserID = student.UserID?student.UserID:await register.addUser(con, SchoolID, student.email)
					return UserID
				})
			)
			resolve(result)
		})
	},

	registerToTeacher: (con, classes, studentIDs) => {
		return new Promise(resolve => {
			let isComplete = 0
			studentIDs.forEach(UserID => {
				classes.forEach(async c => {
					if(await register.isInClass(con, c.ClassFID, UserID)){
						register.addToClass(con, c.ClassFID, UserID).then(UserID => {
							console.log('User ID: "' + UserID + '" successfully added to class: "' + c.ClassFID + '".')
							isComplete++
							register.isComplete(isComplete, classes.length, studentIDs.length, resolve)
						})
					}else{
						console.log('User ID: "' + UserID + '" is already in class: "' + c.ClassFID + '".')
						isComplete++
						register.isComplete(isComplete, classes.length, studentIDs.length, resolve)
					}
				})
			})
		})
	},
	isInClass: (con, ClassID, UserID) => {
		return new Promise(resolve => {
			let query = 
				"SELECT "+
					"ClassFID "+
				"FROM ClassMembers "+
				"WHERE "+
					"UserFID = '" + UserID + "' "+
					"AND ClassFID = '" + ClassID + "' "+
					"AND isDeleted is FALSE"
			;
			QueryEngine.SQL.query(con, query, classes => {
				resolve(classes.result.length === 0)
			})
		})
	},
	isComplete: (isComplete, a, b, resolve) => {
		console.log(isComplete, a * b)
		if(isComplete === a * b){
			resolve()
		}
	},

	addUser: (con, SchoolID, email) => {
		return new Promise(resolve => {
			let query =
				"INSERT INTO Users( "+
					"SchoolFID, "+
					"RoleFID, "+
					"contact, "+
					"email, "+
					"name, "+
					"password, "+
					"isDeleted, "+
					"createdAt, "+
					"lastModified "+
				")VALUES("+
					SchoolID+","+
					3+","+
					"'"+99999999+"',"+
					"'"+email+"',"+
					"'"+email.split("@")[0]+"',"+
					"'123456',"+
					// form.ClassID+","+
					"FALSE,"+
					"NOW(),"+
					"NOW()"+
				")"
			QueryEngine.SQL.queryAdd(con, query, result => {
				resolve(result.result.insertId)
			})
		})
	},
	addToClass: (con, ClassID, UserID) => {
		return new Promise(resolve => {
			let query =
				"INSERT INTO ClassMembers( "+
					"UserFID, "+
					"ClassFID, "+
					"isDeleted, "+
					"createdAt, "+
					"lastModified "+
				")VALUES("+
					UserID+","+
					ClassID+","+
					"FALSE,"+
					"NOW(),"+
					"NOW()"+
				")"
			QueryEngine.SQL.queryAdd(con, query, result => {
				resolve({
					ClassMembersRegID: result.result.insertId,
					ClassID
				})
			})
		})
	}
	
};

module.exports = register;
