const QueryEngine = require('../../QueryEngine');

const suspend= {
	
	init: (req, res) => {
		let form = req.body
		let conID,
			con = QueryEngine.SQL.initSQL()
		QueryEngine.SQL.connect(con, connection => {
			if(connection.error){
				res.status(500).send({
					message: res.error
				})
			}else{
				conID = connection.connection.threadId

				suspend.validateStudent(con, form.student, res).then(student => {
					suspend.suspend(con, student, res).then(result => {
						suspend.endConnection(con, () => {
							res.sendStatus(204)
						})
					})
				}).catch(err=>{})

			}
		})
	},

	suspend: (con, UserID, res) => {
		return new Promise(resolve => {
			suspend.getStudentsClasses(con, UserID, res).then(async classes => {
				let left = await Promise.all(
					classes.map(async c => {
						return await suspend.isSuspendedFromClass(con, UserID, c.ClassFID)
					})
				)
				let result = await Promise.all(
					left.map(async ClassID => {
						if (ClassID) return await suspend.suspendedFromClass(con, UserID, ClassID)
					})
				)
				resolve(result)
			})
		})
	},

	validateStudent: async (con, student, res) => {
		return new Promise((resolve, reject) => {
			let studentExists =
				"SELECT "+
					"UserID "+
				"FROM Users "+
				"WHERE "+
					"email = '" + student + "' "+
					"AND RoleFID > 2"
			;
			QueryEngine.SQL.query(con, studentExists, student => {

				if(student.result.length === 0){
					suspend.endConnection(con, () => {
						reject(false)
						res.status(409).send({
							message: "Request denied. No student!"
						})
					})
				}else{
					resolve(student.result[0].UserID)
				}

			})
		})
	},

	getStudentsClasses: (con, UserID, res) => {
		return new Promise(resolve => {
			let query =
				"SELECT "+
					"ClassFID "+
				"FROM ClassMembers "+
				"WHERE "+
					"UserFID = '" + UserID + "' "+
					"AND isDeleted is FALSE"
			;
			QueryEngine.SQL.query(con, query, classes => {

				if(classes.result.length === 0){
					suspend.endConnection(con, () => {
						res.status(409).send({
							message: "Request denied. Student has no classes!"
						})
					})
				}else{
					resolve(classes.result)
				}

			})
		})
	},
	isSuspendedFromClass: (con, student, ClassID) => {
		return new Promise(resolve => {
			let query =
				"SELECT "+
					"UserFID "+
				"FROM SuspendedStudents "+
				"WHERE "+
					"UserFID = '" + student + "' "+
					"AND ClassFID = '" + ClassID + "' "+
					"AND isDeleted is FALSE"
			;
			QueryEngine.SQL.query(con, query, result => {

				resolve(result.result.length === 0?ClassID:false)

			})
		})
	},
	suspendedFromClass: (con, UserID, ClassID) => {
		return new Promise(resolve => {
			let query =
				"INSERT INTO SuspendedStudents( "+
					"UserFID, "+
					"ClassFID, "+
					"isDeleted, "+
					"createdAt, "+
					"lastModified "+
				")VALUES("+
					UserID+","+
					ClassID+","+
					"FALSE,"+
					"NOW(),"+
					"NOW()"+
				")"
			QueryEngine.SQL.queryAdd(con, query, result => {
				resolve({
					CaseID: result.result.insertId
				})
			})
		})
	},

	endConnection: (con, handle) => {
		QueryEngine.SQL.end(con, () => {
			handle()
		})
	}
	
};

module.exports = suspend;
