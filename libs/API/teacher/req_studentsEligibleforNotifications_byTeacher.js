const QueryEngine = require('../../../QueryEngine');

const req_usersSameClassAs_UserID= {
	
	init: (req, handle) => {
		let form = req.body
		QueryEngine.SQL.once(
			"SELECT cm.ClassFID FROM ClassMembers cm "+
			"WHERE "+
				"cm.UserFID = '" + form.TeacherID + "' "+
				"AND cm.isDeleted is FALSE"
		, classRes => {
			let Classes = classRes.result;
			let isComplete = {}
			Classes.forEach(obj => {
				isComplete[obj.ClassFID] = null
				QueryEngine.SQL.once(
					"SELECT DISTINCT "+
						"u.UserID, "+
						"u.contact, "+
						"u.name, "+
						"u.email, "+
						"u.lastModified "+
					"FROM Users u, ClassMembers cm "+
					"WHERE "+
						"u.UserID = cm.UserFID "+
						"AND u.RoleFID > 2 "+
						"AND u.isDeleted is FALSE "+
						"AND cm.isDeleted is FALSE "+
						"AND cm.UserFID NOT IN ( "+
							"SELECT ss.UserFID FROM SuspendedStudents ss WHERE ss.isDeleted is FALSE "+
						") "+
						"AND cm.ClassFID = '" + obj.ClassFID + "' "+
					"ORDER BY u.UserID"
				, classMembersRes => {
					isComplete[obj.ClassFID] = classMembersRes.result
					req_usersSameClassAs_UserID.isComplete(isComplete, handle)
				})
			})
		})
	},

	isComplete: (obj, handle) => {
		let pass = true
		Object.keys(obj).map(i => {
			if(!obj[i]){
				pass = false
			}
		})
		if(pass) handle(obj)
	}
	
};

module.exports = req_usersSameClassAs_UserID;
